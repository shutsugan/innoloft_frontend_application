import React, { useContext, useState } from "react";
import PropTypes from "prop-types";

import { AppContext } from "../App/App";
import Message from "../Message";
import Field from "../Field";

import "./MainForm.css";

const MainForm = ({ updateData }) => {
  const { userData, setter } = useContext(AppContext);

  const [email, setEmail] = useState(userData.email);
  const [password, setPassword] = useState("");
  const [rePassword, setRePassword] = useState("");

  const [error, setError] = useState("");

  const SaveData = () => {
    setter({
      ...userData,
      email,
      password,
      success: "Data saved successfully",
    });
  };

  const SendData = () => {
    updateData(
      {
        ...userData,
        email,
        password,
      },
      () => setter({ ...userData, success: "Data updated successfully" })
    );
  };

  const removeMessage = () => {
    setter({ ...userData, success: "" });
  };

  const checkEmail = () => {
    const re = /\S+@\S+\.\S+/;
    if (!re.test(email)) setError({ email: "Email is not valid" });
    else setError("");
  };

  const checkPassword = () => {
    if (password !== rePassword)
      setError({ password: "Passwords should match" });
    else setError("");
  };

  const checkStrength = [/[$@$!%*#?&]/, /[A-Z]/, /[0-9]/, /[a-z]/].reduce(
    (memo, test) => memo + test.test(password),
    0
  );

  return (
    <div className="main-form w-full">
      <Message success={userData.success} removeMessage={removeMessage} />

      {!userData.success && (
        <>
          <Field
            type="email"
            name="email"
            label="E-Mail"
            value={email}
            onChange={({ target }) => setEmail(target.value)}
            onBlur={checkEmail}
            error={error["email"]}
            placeholder="Change e-mail address"
          />
          <Field
            type="password"
            name="password"
            label="Password"
            value={password}
            onChange={({ target }) => setPassword(target.value)}
            onBlur={() => rePassword && checkPassword()}
            placeholder="****"
          />
          <div className="progress-wrapper">
            <div
              className="progress"
              style={{ width: `${checkStrength * 20 + 20}%` }}
            />
          </div>
          <Field
            type="password"
            name="re-password"
            label="Password repeat"
            value={rePassword}
            onChange={({ target }) => setRePassword(target.value)}
            onBlur={checkPassword}
            error={error["password"]}
            placeholder="****"
          />
          <div
            className={`form-buttons flex space-between align-center ${
              error || !password || !rePassword ? "disabled" : ""
            }`}
          >
            <button className="button" onClick={SaveData}>
              Save
            </button>
            <button className="button" onClick={SendData}>
              Update
            </button>
          </div>
        </>
      )}
    </div>
  );
};

MainForm.propTypes = {
  updateData: PropTypes.func,
};

export default MainForm;
