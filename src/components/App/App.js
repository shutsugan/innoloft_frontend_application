import React, { useState } from "react";

import Header from "../Header";
import Aside from "../Aside";
import Dashboard from "../Dashboard";

import "./App.css";

const initData = {
  email: "shutsugan@gmail.com",
  password: "Last-Pass1!",
  firstName: "Mohamed",
  lastName: "Tajjiou",
  country: "Morocco",
  address: {
    street: "Rue h2",
    house: "A",
    number: "30",
    postalCode: "60020",
  },
  success: "",
};

let AppContext = null;

const App = () => {
  const [userData, setUserDate] = useState(initData);
  const context = {
    userData,
    setter: (payload) => setUserDate(payload),
  };

  AppContext = React.createContext(context);

  return (
    <AppContext.Provider value={context}>
      <div className="app w-full flex flex-col">
        <Header />
        <div className="app-content w-full flex p-1">
          <Aside />
          <Dashboard />
        </div>
      </div>
    </AppContext.Provider>
  );
};

export { AppContext };
export default App;
