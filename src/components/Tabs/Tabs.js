import React from "react";
import PropTypes from "prop-types";

import "./Tabs.css";

const Tabs = ({ tabs, selected, setTab }) => (
  <div className="w-full flex flex-col">
    <div className="flex">
      {tabs.map(({ tab, title, content }) => (
        <div
          key={tab}
          className={`tab flex flex-1 align-center justify-center p-1 ${
            selected.tab === tab ? "tab-active" : ""
          }`}
          onClick={() => setTab({ tab, content })}
        >
          {title}
        </div>
      ))}
    </div>
    <div className="tab-content flex align-center w-full p-1">
      {selected.content}
    </div>
  </div>
);

Tabs.propTypes = {
  tabs: PropTypes.array,
  selected: PropTypes.object,
  setTab: PropTypes.func,
};

export default Tabs;
