import React, { useContext, useState } from "react";
import PropTypes from "prop-types";

import { AppContext } from "../App/App";
import Message from "../Message";
import Field from "../Field";

const AdditionalForm = ({ updateData }) => {
  const { userData, setter } = useContext(AppContext);

  const [firstName, setFirstName] = useState(userData.firstName);
  const [lastName, setLastName] = useState(userData.lastName);
  const [address, setAddress] = useState(userData.address);
  const [country, setCoutry] = useState(userData.country);

  const SaveData = () => {
    setter({
      ...userData,
      firstName,
      lastName,
      address,
      country,
      success: "Data saved successfully",
    });
  };

  const SendData = () => {
    updateData(
      {
        ...userData,
        firstName,
        lastName,
        address,
        country,
      },
      () => setter({ ...userData, success: "Data updated successfully" })
    );
  };

  const removeMessage = () => {
    setter({ ...userData, success: "" });
  };

  return (
    <div className="additional-form w-full">
      <Message success={userData.success} removeMessage={removeMessage} />

      {!userData.success && (
        <>
          <Field
            type="text"
            name="firstName"
            label="First Name"
            value={firstName}
            onChange={({ target }) => setFirstName(target.value)}
            placeholder="Change first name"
          />

          <Field
            type="text"
            name="lastName"
            label="Last Name"
            value={lastName}
            onChange={({ target }) => setLastName(target.value)}
            placeholder="Change last name"
          />

          <Field
            type="text"
            name="county"
            label="Country"
            value={country}
            onChange={({ target }) => setCoutry(target.value)}
            placeholder="Change street"
          />

          <Field
            type="text"
            name="street"
            label="Street"
            value={address.street}
            onChange={({ target }) =>
              setAddress({ ...address, street: target.value })
            }
            placeholder="Change country"
          />

          <Field
            type="text"
            name="house"
            label="House"
            value={address.house}
            onChange={({ target }) =>
              setAddress({ ...address, house: target.value })
            }
            placeholder="Change house"
          />

          <Field
            type="number"
            name="number"
            label="Number"
            value={address.number}
            onChange={({ target }) =>
              setAddress({ ...address, number: target.value })
            }
            placeholder="Change number"
          />

          <Field
            type="text"
            name="postalCode"
            label="Postal Code"
            value={address.postalCode}
            onChange={({ target }) =>
              setAddress({ ...address, postalCode: target.value })
            }
            placeholder="Change postal code"
          />

          <div className="form-buttons flex space-between align-center">
            <button className="button" onClick={SaveData}>
              Save
            </button>
            <button className="button" onClick={SendData}>
              Update
            </button>
          </div>
        </>
      )}
    </div>
  );
};

AdditionalForm.propTypes = {
  updateData: PropTypes.func,
};

export default AdditionalForm;
