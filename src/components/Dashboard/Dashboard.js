import React, { useState } from "react";
import axios from "axios";

import Tabs from "../Tabs";
import MainForm from "../MainForm";
import AdditionalForm from "../AdditionalForm";

import "./Dashboard.css";

const Dashboard = () => {
  const updateData = async (payload, done) => {
    await axios.post("https://postman-echo.com/post", payload);
    done();
  };

  const tabs = [
    {
      tab: "main",
      title: "Main Information",
      content: <MainForm updateData={updateData} />,
    },
    {
      tab: "additional",
      title: "Additional Information",
      content: <AdditionalForm updateData={updateData} />,
    },
  ];

  const [selectedTab, setSelectedTab] = useState(tabs[0]);

  return (
    <div className="dashboard">
      <Tabs tabs={tabs} selected={selectedTab} setTab={setSelectedTab} />
    </div>
  );
};

export default Dashboard;
