import React from "react";
import PropTypes from "prop-types";

const Message = ({ success, removeMessage }) => (
  <>
    {success && (
      <div
        className="success p-1 flex align-center space-between"
        onClick={removeMessage}
      >
        <span>{success}</span> <span>x</span>
      </div>
    )}
  </>
);

Message.propTypes = {
  success: PropTypes.string,
  removeMessage: PropTypes.func,
};

export default Message;
