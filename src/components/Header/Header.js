import React from "react";
import PropTypes from "prop-types";

import { ReactComponent as GlobeIcon } from "../../Icons/globe.svg";
import { ReactComponent as EnvelopIcon } from "../../Icons/envelop.svg";
import { ReactComponent as BellIcon } from "../../Icons/bell.svg";

import "./Header.css";

const Logo = () => (
  <div className="logo flex align-center">
    <span className="mb-hide">
      ENER<span className="logo-g">G</span>IE
    </span>
    <span className="mb-show">
      E<span className="logo-g">G</span>
    </span>
    <span className="logo-slug">LOFT</span>
  </div>
);

const MenuItem = ({ icon, text, notif }) => (
  <a className="menu-items flex align-center relative" href="#globe">
    {icon} {text}
    {notif && <span className="menu-notif absolute" />}
  </a>
);

const Header = () => {
  return (
    <div className="header flex align-center space-between p-1">
      <Logo />
      <div className="header-menu flex">
        <MenuItem icon={<GlobeIcon />} text="EN" />
        <MenuItem icon={<EnvelopIcon />} notif />
        <MenuItem icon={<BellIcon />} notif />
      </div>
    </div>
  );
};

MenuItem.propTypes = {
  icon: PropTypes.node,
  text: PropTypes.string,
  notif: PropTypes.bool,
};

export default Header;
