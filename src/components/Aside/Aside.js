import React from "react";
import PropTypes from "prop-types";

import { ReactComponent as HomeIcon } from "../../Icons/home.svg";
import { ReactComponent as AnnouceIcon } from "../../Icons/annouce.svg";
import { ReactComponent as CompanyIcon } from "../../Icons/company.svg";
import { ReactComponent as SettingIcon } from "../../Icons/settings.svg";
import { ReactComponent as NewsIcon } from "../../Icons/news.svg";
import { ReactComponent as ChartIcon } from "../../Icons/chart.svg";

import "./Aside.css";

const AsideItem = ({ icon, title }) => (
  <a className="aside-item flex align-center" href={title.toLowerCase()}>
    {icon} {title}
  </a>
);

const Aside = () => (
  <div className="aside flex flex-col align-center">
    <AsideItem icon={<HomeIcon />} title="Home" />
    <AsideItem icon={<AnnouceIcon />} title="Account" />
    <AsideItem icon={<CompanyIcon />} title="Company" />
    <AsideItem icon={<SettingIcon />} title="Setting" />
    <AsideItem icon={<NewsIcon />} title="News" />
    <AsideItem icon={<ChartIcon />} title="Analytics" />
  </div>
);

AsideItem.propTypes = {
  icon: PropTypes.node,
  title: PropTypes.string,
};

export default Aside;
