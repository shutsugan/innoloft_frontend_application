import React from "react";
import PropTypes from "prop-types";

import "./Field.css";

const Field = ({ name, type, label, error, ...rest }) => {
  return (
    <div className="field flex flex-col">
      <label htmlFor={name} className="field-label">
        {label}
      </label>
      <input
        className={`field-input ${error ? "border-error" : ""}`}
        type={type}
        id={name}
        {...rest}
      />
      {error && <span className="field-error">{error}</span>}
    </div>
  );
};

Field.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  label: PropTypes.string,
  error: PropTypes.string,
  rest: PropTypes.object,
};

export default Field;
